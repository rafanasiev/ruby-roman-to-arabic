#!/usr/bin/env ruby
#
# converter.rb - a tool to convert Roman numbers into Arabic,
#                and vice-versa
#
#   Author::    Ruslan A. Afanasiev  (mailto:ruslan.afanasiev@gmail.com)
#   License::   Distributes under the same terms as Ruby
# -------------------------------------------------------------------
# -*- encoding: utf-8 -*-

lib = File.absolute_path(__FILE__).gsub!(/bin\/\w+.rb/, 'lib')
$LOAD_PATH.unshift(lib)

require 'getoptions'
require 'pp'
require 'numbers/roman'

def usage
  print <<USAGE
Usage: #{$0} [OPTIONS] ... [ARGS]
Converts Roman numbers into Arabic. Also, it does vice-versa conversation.
    --to-roman   convert Arabic numbers into Roman
    --to-arabic  convert Roman numbers into Arabic

    --debug     prints debug information
    --help      display this help and exit
USAGE
  exit(0)
end


## check command line agrs
ARGV << "-h" if ARGV.empty?

## define CLI arguments and options
opts = begin
  GetOptions.new( %w{help usage debug! to-roman=i to-arabic=s} )
rescue GetOptions::ParseError => e
  puts "CL argument error - " + e.message
  usage
end

if opts.debug
  puts " -- DEBUG mode is ON."
  ENV["DEBUG"] = "1"
  puts opts.inspect
end

## check CLI agrs
# - print usage if help OR usage
opts.each do |opt, value|
    # result - it takes results of Numerical transformation 
    result = case opt
      when "to-roman"
        puts " -- Trying to convert <#{value}> into Roman ..."
        Numbers::Roman.to_roman(value)
      when "to-arabic"
        puts " -- Trying to convert <#{value.upcase}> into Arabic ..."
        Numbers::Roman.to_arabic(value)
      else
        usage
    end

    # cannot transform Roman-to-Arabic or vice-versa
    if result.nil?
      raise " -- Cannot convert <#{value}>, please check the input!"
    else
      puts " -- Result: #{result}"
      exit(0)
    end
end
