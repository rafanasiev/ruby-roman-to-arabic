module Numbers

  require 'basenum'

  class Roman < BaseNum
    VERSION = "0.1.0"
    # map Arabic digits to their Roman literals
    ROMAN_LITERALS = {
        1   => "I",
        4   => "IV",
        5   => "V",
        9   => "IX",
        10  => "X",
        40  => "XL",
        50  => "L",
        90  => "XC",
        100 => "C",
        400 => "CD",
        500 => "D",
        900 => "CM",
       1000 => "M",

    }

    # Based on Roman regex, "Perl Cookbook"
    REGEX = /^M*(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$/i

    def self.is_number_roman?(string)
      REGEX =~ string
    end

    def self.to_roman(value)
      if value.is_a?(Fixnum) and value > 0

        ROMAN_LITERALS.keys.sort{|a,b| b <=> a}.inject("") do |result, key|
          multi, value = value.divmod(key)

          if ENV["DEBUG"]
            puts <<EOL
-------------------
---    Multiplexer: #{multi}
--- Current number: #{value}
---            KEY: #{key}
---      ROMAN NUM: #{ROMAN_LITERALS[key]}
EOL
          end
          # ----
          # NOTE: don't put "an empty string" into result []
          #       if multi == 0, go to the next iteration
          # ----
          multi.zero? ? result : result << ROMAN_LITERALS[key] * multi
        end

      else
        return nil
      end
    end

    def self.to_arabic(value)
      return nil unless is_number_roman?(value)

      if ENV["DEBUG"]
          puts "--- Roman Number: #{value} - is correct."
      end

      roman_to_digit = ROMAN_LITERALS.invert
      last = nil
      value.upcase.split('').reverse.inject(0) do |sum, roman|
          if ENV["DEBUG"]
            puts "------------------"
            puts "---   Roman Sigil: #{roman}"
            puts "--- Arabic Number: #{roman_to_digit[roman]}"
            puts "---           SUM: #{sum}"
          end

          if digit = roman_to_digit[roman].to_i

            if last && last > digit
              sum -= digit
            else
              sum += digit
            end
            # memoize last digit
            last = digit
          end
          # return
          sum
      end
    end


  end

end
